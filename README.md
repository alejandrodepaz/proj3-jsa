Name: Alejandro De Paz
Contact: adepaz@uoregon.edu

This program generates a simple vocabulary anagram game. flask_vocab.py
operates in the backend, sending appropriate JSON data to vocab.html depending
on the request made by the user. vocab.html utilizes AJAX to complete the form
interaction seen on the frontend to create a continuous interaction with the 
user upon each keystroke.

 
